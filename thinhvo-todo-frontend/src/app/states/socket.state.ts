import { WSMessage } from '../todo/todo.model';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
 
export interface SocketState {
  socketObject: WebSocketSubject<WSMessage>;
}
