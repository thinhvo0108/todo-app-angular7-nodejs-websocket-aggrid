import { Todo } from '../todo/todo.model';
 
export interface TodoState {
  fullList: Todo[];
}
