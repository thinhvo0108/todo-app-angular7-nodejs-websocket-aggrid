import {TodoState} from './todo.state';
import { SocketState } from './socket.state';
 
export interface AppState {
  todo: TodoState;
  socket: SocketState;
}
