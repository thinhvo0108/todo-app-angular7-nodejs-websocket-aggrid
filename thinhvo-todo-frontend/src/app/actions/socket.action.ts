import { Action } from '@ngrx/store';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { WSMessage } from '../todo/todo.model';

export namespace SocketActionTypes {
  export const UPDATE_SOCKET_OBJECT = '[Socket] update_socket_object';  
}
 
export class SocketUpdateSocketObjectAction implements Action {
  readonly type = SocketActionTypes.UPDATE_SOCKET_OBJECT;
  public payload: WebSocketSubject<WSMessage>;
 
  constructor(public socket: WebSocketSubject<WSMessage>) {
    this.payload = socket;
  }
}
 
export type SocketAction = SocketUpdateSocketObjectAction;
