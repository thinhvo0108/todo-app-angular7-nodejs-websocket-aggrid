import { Action } from '@ngrx/store';
import { Todo } from '../todo/todo.model';

export namespace TodoActionTypes {
  export const ADD = '[Todo] add';
  export const UPDATE_FULL_LIST = '[Todo] update_full_list';  
}
 
export class TodoAddAction implements Action {
  readonly type = TodoActionTypes.ADD;
  public payload: Todo;
 
  constructor(public todo: Todo) {
    this.payload = todo;
  }
}
 
export class TodoUpdateFullListAction implements Action {
  readonly type = TodoActionTypes.UPDATE_FULL_LIST;
  public payload: Todo[];
 
  constructor(public fullList: Todo[]) {
    this.payload = fullList;
  }
}
 
export type TodoAction = TodoAddAction | TodoUpdateFullListAction;
