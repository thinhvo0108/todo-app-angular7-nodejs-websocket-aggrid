import {Component, OnInit} from "@angular/core";
import { Todo, WSMessage } from '../todo/todo.model';
import { AppState } from '../states/app.state';
import { select, Store } from '@ngrx/store';
import { GridOptions } from "ag-grid-community";
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { TodoUpdateFullListAction } from '../actions/todo.action';
import { SocketUpdateSocketObjectAction } from '../actions/socket.action';

const RECONNECT_INTERVAL = 1000;

const ACTION_TYPES = {
    SERVER: {
        UPDATE_FULL_LIST: '[Server] update full list',
    },
    CLIENT: {
        ADD_TODO: '[Client] add todo',
        FINISH_TODO: '[Client] finish todo',
    },
  }

@Component({
    selector: 'ag-grid-todo',
    templateUrl: './ag-grid-todo.component.html',
})
export class AgGridTodoComponent implements OnInit{
    socketObject: WebSocketSubject<WSMessage>
    fullList: Todo[];
    gridOptions: GridOptions; 
    reconnectInterval: number;

    constructor(private store: Store<AppState>) {
        this.store.pipe(select(state => state))
        .subscribe(state => {
          if (state.todo && state.todo.fullList) {
            this.fullList = state.todo.fullList;
          }
          if (state.socket && state.socket.socketObject) {
            this.socketObject = state.socket.socketObject;
          }        
        });  
             
        this.gridOptions = <GridOptions> {
            deltaRowDataMode: true,       
            getRowNodeId: (data) => {
                return data.id;
            },
            onFirstDataRendered(params) {
                params.api.sizeColumnsToFit();
            },
            onRowDoubleClicked: ({data: {id, name, note}}) => {
                this.finishTodoOnWSServer(id, name, note);
            },
        };
    }

    handleWSConnectionError = (err) => {
        this.socketObject = null;
        clearInterval(this.reconnectInterval);
        this.reconnectInterval = window.setInterval(this.reconnect, RECONNECT_INTERVAL);
        // console.log(err); // In Production: send email or SMS to admin
      }
    
      reconnect = () => {
        if ((this.socketObject instanceof WebSocketSubject)) {
          return;
        }
        this.connect();
      }  
    
      connect = () => {
        this.socketObject = new WebSocketSubject('ws:localhost:8080');
    
        this.socketObject.subscribe(
          (message) => this.store.dispatch(new TodoUpdateFullListAction(message.data)),
          this.handleWSConnectionError,
        );    
    
        if ((this.socketObject instanceof WebSocketSubject)) {
          this.store.dispatch(new SocketUpdateSocketObjectAction(this.socketObject))     
        }
      }
    
      ngOnInit(): void {
        this.connect();
      }   
      
    // Instead of dispatching an action to update redux store (by deleting a finished Todo item), send the finished todo item to ws server
    finishTodoOnWSServer(id, name, note) {
        if (Boolean(this.socketObject)) {
            this.socketObject.next({
                type: ACTION_TYPES.CLIENT.FINISH_TODO,
                data: [{id, name, note}],
            });
        } 
    }      

    columnDefs = [
        { headerName: 'Name', field: 'name', sortable: true, filter: true },
        { headerName: 'Note', field: 'note', sortable: true, filter: true },
    ];
    
}
