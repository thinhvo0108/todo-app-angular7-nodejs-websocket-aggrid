import { Todo, WSMessage } from './todo.model';
import { AppState } from '../states/app.state';
import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';

const ACTION_TYPES = {
  SERVER: {
      UPDATE_FULL_LIST: '[Server] update full list',
  },
  CLIENT: {
      ADD_TODO: '[Client] add todo',
      FINISH_TODO: '[Client] finish todo',
  },
}

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  todoForm: FormGroup;
  socketObject: WebSocketSubject<WSMessage>;
  fullList: Todo[];

  constructor(private store: Store<AppState>, private fb: FormBuilder) {
    this.createForm();
    this.store.pipe(select(state => state.socket))
      .subscribe(socket => {
        if (socket && socket.socketObject) {
          this.socketObject = socket.socketObject;
        }
      });      
    this.store.pipe(select(state => state.todo))
      .subscribe(todo => {
        if (todo && todo.fullList) {
          this.fullList = todo.fullList;
        }
      });
  }

  createForm() {
    this.todoForm = this.fb.group({
      name: ['', Validators.required ],
   });
  }

  // Instead of dispatching an action to update redux store (by adding new Todo item), send the new todo item to ws server
  sendNewTodoToWSServer(name, note) {
    if (Boolean(this.socketObject)) {
      this.socketObject.next({
        type: ACTION_TYPES.CLIENT.ADD_TODO,
        data: [{name, note}],
      });
    } 
  }

  ngOnInit() { }
}
