export interface Todo {
  id?: number,
  name: string;
  note?: string;
}

export interface WSMessage {
  type: string;
  data: Todo[];
}
