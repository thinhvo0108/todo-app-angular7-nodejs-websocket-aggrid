import { TodoState } from '../states/todo.state';
import { TodoAction, TodoActionTypes } from '../actions/todo.action';
 
const initialTodoState: TodoState = {
  fullList: []
};
 
export function todoActionReducer(state: TodoState = initialTodoState, action: TodoAction): TodoState {
  switch (action.type) {
    case TodoActionTypes.UPDATE_FULL_LIST:
      state.fullList = action.payload;
      return {
        fullList: state.fullList
      };
  }
}
