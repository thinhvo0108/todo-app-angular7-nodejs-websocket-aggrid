import { SocketState } from '../states/socket.state';
import { SocketAction, SocketActionTypes } from '../actions/socket.action';
 
const initialTodoState: SocketState = {
  socketObject: null
};
 
export function socketActionReducer(state: SocketState = initialTodoState, action: SocketAction): SocketState {
  switch (action.type) {
    case SocketActionTypes.UPDATE_SOCKET_OBJECT:
      state.socketObject = action.payload;
      return {
        socketObject: state.socketObject
      };
  }
}
