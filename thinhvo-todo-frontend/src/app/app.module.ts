import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { AppComponent } from './app.component';
import { TodoComponent } from './todo/todo.component';
import { todoActionReducer } from './reducers/todo.reducer';
import { socketActionReducer } from './reducers/socket.reducer';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from "ag-grid-angular";
import { AgGridTodoComponent } from "./ag-grid-todo-component/ag-grid-todo.component";

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    AgGridTodoComponent,
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({todo: todoActionReducer, socket: socketActionReducer}),
    ReactiveFormsModule,
    AgGridModule.withComponents([]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
