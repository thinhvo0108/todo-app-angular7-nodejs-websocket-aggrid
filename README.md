# ThinhvoTodo

[Sorry I don't have enough time for typing the README correctly, so I just type here how to run the app]

## How to run this project

### Backend: Node.js - Websocket:
- npm install
- node app.js
=> It will run on port 8080

### Frontend: Angular 7 - NgRx (for state management) - ag Grid (powerful JS grid)
- npm install
- ng serve
=> it will run on port 4200, please open your browser and type http://localhost:4200/ to run the app

#### There is auto-reconnect feature in the frontend side 