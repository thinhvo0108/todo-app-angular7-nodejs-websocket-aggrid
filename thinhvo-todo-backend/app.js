"use strict";
const express = require("express");
const http = require("http");
const WebSocket = require("ws");

const ACTION_TYPES = {
    SERVER: {
        UPDATE_FULL_LIST: '[Server] update full list',
    },
    CLIENT: {
        ADD_TODO: '[Client] add todo',
        FINISH_TODO: '[Client] finish todo',
    },
}

var db = [
    {
        id: 1,
        name: 'Websocket',
        note: 'definition and comparision to others',
    },
    {
        id: 2,
        name: 'agGrid',
        note: 'community edition',
    },            
];
var index = 2; // For auto-increment ID

function createAndStartAppServer() {
    var app = express();

    // Initialise http server
    var server = http.createServer(app);

    // Initialise ws instance
    var wss = new WebSocket.Server({ server: server });

    // Broadcast function
    wss.broadcast = function broadcast(data) {
        wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
                client.send(data);
            }
        });
    };    

    // After connection is established, main logics go here
    wss.on('connection', function (ws) {
        console.log("Connection Established - Sending Current DB");

        var outcomingMessage = {
            type: ACTION_TYPES.SERVER.UPDATE_FULL_LIST,
            data: db,
        }
        var incomingMessageData;
        var todoItem;

        // Not just sending to 1 client, but broadcast everyone
        // ws.send(JSON.stringify(outcomingMessage)); 
        wss.broadcast(JSON.stringify(outcomingMessage));

        // Receive message from client 
        ws.onmessage = function(incomingMessage) {
            incomingMessageData = JSON.parse(incomingMessage.data);
            switch(incomingMessageData.type) {
                case ACTION_TYPES.CLIENT.ADD_TODO:
                    
                    todoItem = incomingMessageData.data[0];
                    todoItem.id = ++index;

                    db.push(todoItem);
                
                    outcomingMessage = {
                        type: ACTION_TYPES.SERVER.UPDATE_FULL_LIST,
                        data: db,
                    }         
                    
                    // Not just sending to 1 client, but broadcast everyone
                    // ws.send(JSON.stringify(outcomingMessage));    
                    wss.broadcast(JSON.stringify(outcomingMessage));                
                    break;
                case ACTION_TYPES.CLIENT.FINISH_TODO:
                    todoItem = incomingMessageData.data[0];

                    if (todoItem.id) {
                        for( var i = 0; i < db.length; i++){ 
                            if ( db[i].id === todoItem.id) {
                              db.splice(i, 1); 
                              i--;
                            }
                         }                        
                    }
                
                    outcomingMessage = {
                        type: ACTION_TYPES.SERVER.UPDATE_FULL_LIST,
                        data: db,
                    }         
                    
                    // Not just sending to 1 client, but broadcast everyone
                    // ws.send(JSON.stringify(outcomingMessage));    
                    wss.broadcast(JSON.stringify(outcomingMessage));  

                    break;
                default:
                    break;
            }
        };       
    });
 
    // Start the server
    server.listen(process.env.PORT || 8080, function () {
        var port = server.address().port;
        console.log("Server started on port " + port);
    });
}

createAndStartAppServer();
